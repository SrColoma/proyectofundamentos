import miLib as f
import random as rd

# Inicializando datos
partidas_jugadas = []
puntajes_partidas = []
turnos = 5
numFichas = 4
contadorPartidas = 0
usuarios = []
# Presentando el menu
opcion = f.menu()

while opcion != "4":
    if (opcion == "1"):
        puntaje = 0
        tmpUser = input("Ingrese su nombre: ")
        # Creamos el mazo con n-1 fichas porque en cada iteración agregamos una nueva ficha aleatoria
        mazo = f.generarFichas(numFichas - 1)
        mazo = f.generarMazoNP(mazo);
        tablero = []
        # Este for iterará las veces correspondientes a los turnos
        for j in range(turnos):
            # Creamos una ficha nueva en cada turno y la guardamos en el tablero
            ficha = f.validarRepeticion(str(rd.randint(1, 9)) + ";" + str(rd.randint(1, 9)), tablero)
            tablero.append(ficha)

            # Creamos una nueva ficha aleatoria para el mazo
            #ficha_mazo = f.validarRepeticion(str(rd.randint(1, 9)) + ";" + str(rd.randint(1, 9)), mazo)
            #mazo.append(ficha_mazo)

            print("\n\n**************** TURNO ", j + 1, " *******************")
            #print("Mazo: ", mazo)
            #print("Tablero: ", tablero)
            #print("Puntaje: ", puntaje)

            # Pedimos al usuario ingresar una ficha del maso y validamos que se encuentre
            #ficha_ingresada = f.validarRango(int(f.validarIngreso(input("Ingrese una ficha de su mazo(posición): "))),
            #                                mazo)
            mazo = f.agregarFichaMazo(mazo);
            direccion=""
            while direccion != "-1":
                f.imprimirAvance(mazo,tablero,puntaje)
                direccion = input("Ingrese derecha o izquierda d/i: ")
                puntaje += f.calculaPuntaje(mazo, tablero, direccion)

            # if ficha_ingresada != -1:
            #     # Buscamos la ficha del mazo que está en la posición ingresada.
            #     ficha_User = mazo[ficha_ingresada - 1]
            #     # Llamamos a la funcion validarFichha para que nos devuelva la posición en la que se encuentra la ficha dentro del tablero
            #     posicion = f.validarFicha(tablero, ficha_User)
            #     # Si no coinciden, se muestra el sgt mensaje
            #     if posicion == -1:
            #         print("La ficha {} no coincide con ninguna del mazo".format(ficha_User))
            #
            #     # Si coinciden se aumenta el puntaje, y se elimina la ficha del mazo y tablero
            #     else:
            #         puntaje += 1
            #         mazo.remove(ficha_User)
            #         tablero.pop(posicion)
            # else:
            #     print("Aún tienes ", abs(j - 4), " turnos!!\n")

            # En la última iteración se presentará toda la información de la partida
            if j == turnos - 1:
                print("\nFIN DE LA PARTIDA. ")
                print("Puntaje: ", puntaje)
                print("Mazo: ", mazo)
                print("Tablero: ", tablero)
        usuarios.append(tmpUser)
        contadorPartidas += 1
        # Se guarda el puntaje de cada partida en listas
        partidas_jugadas.append("Partida " + str(contadorPartidas))
        puntajes_partidas.append(puntaje)
        f.mostratReporte(usuarios,puntajes_partidas)

        opcion = f.menu()



    elif opcion == "2":
        turnos = f.validarIngreso(input("Ingrese la cantidad de turnos que desea para la partida: "))
        opcion = f.menu()

    elif opcion == "3":
        numFichas = f.validarIngreso(input("Ingrese el número de fichas que desea generar en el mazo: "))
        opcion = f.menu()



    else:
        print("*****************  Opción no valida  ***********")

print("Fin del juego. ")
f.ordenarPartidas(partidas_jugadas, puntajes_partidas)
