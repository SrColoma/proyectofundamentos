import random as rd
import numpy as np

def generarFichas(numero=5):
    mazo=[]
    ficha=str(rd.randint(1, 9)) + ";" + str(rd.randint(1, 9))
    for i in range(numero):
        while ficha in mazo:
            ficha= str(rd.randint(1, 9)) + ";" + str(rd.randint(1, 9))
        mazo.append(ficha)

    return mazo


def menu():
    menu = """
     _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
    |             MENU             |
    |1.     Jugar una partida      |
    |2. Ingresar número de turnos  | 
    |3. Ingresar número de fichas  |
    |4.          Salir             |
    | _ _ _ _ _ _ _ _ _ _ _ _ _ _ _|
        """
    print(menu)
    opcion= input("\nIngrese una opción del menú: ")
    return opcion


def validarIngreso(numero):
    error=1
    while error==1:
        if(numero.isalpha()):
            numero=input("Ha ingresado un dato inválido. Vuelva a intentarlo: ")
            error=1
        else:
            numero= int(numero)
            error=0
    return numero


# def validarRepeticionNP(ficha, mazo):
#     for nummazo in range(0, len(mazo), 2):
#         if(mazo[nummazo] == int(ficha[0]) and mazo[nummazo+1] == int(ficha[2])):
#             ficha = str(rd.randint(1, 9)) + ";" + str(rd.randint(1, 9))
#     return ficha

def validarRepeticion(ficha, lista):
    while (ficha in lista ):
        ficha=str(rd.randint(1,9))+";"+str(rd.randint(1,9))
    return ficha

def validarFicha(tablero, ficha):
    elementos= ficha.split(";")
    for i in range(len(tablero)):
        if elementos[0] in tablero[i] or elementos[1] in tablero[i]:
            return i
    return -1

def ordenarPartidas(partidas, puntajes):
    puntajes_copia=puntajes[:]
    puntajes_copia.sort(reverse=True)
    partidas_ordenadas=[]
    #Ordenando lista de partidas
    for i in range(len(partidas)):
        if(partidas[puntajes.index(puntajes_copia[i])] not in partidas_ordenadas):
            partidas_ordenadas.append(partidas[puntajes.index(puntajes_copia[i])])

    #Presentando datos
    for i in range(len(partidas_ordenadas)):
        print(partidas_ordenadas[i]+": "+ str(puntajes_copia[i]))

def validarRango(ficha_ingresada, mazo):
    # Validamos que el indice ingresado este en el rango corecto
    while ficha_ingresada < -1 or ficha_ingresada > len(mazo) or ficha_ingresada == 0:
        ficha_ingresada = int(validarIngreso(
            input("La posición ingresada está fuera del rango\nVuelva a ingresar una ficha de su mazo(posición): ")))
    return ficha_ingresada

def generarMazoNP(mazo):
    numberList=[]
    for ficha in mazo:
        numberList.append(int(ficha[0]))
        numberList.append(int(ficha[2]))
    mazonuevo = np.array(numberList)
    return mazonuevo

def generarFichaNP(ficha):
    if ficha == []:
        return []
    arr = np.array([int(ficha[0]),int(ficha[2])])
    return arr

def  calculaPuntaje(mazoNP,tablero,direccion):

        sumadorP = 0
        if direccion == "-1":
            return 0
        elif direccion == "i" or direccion == "I" :
            poss = 0
            alado = 1
        elif direccion == "d" or direccion == "D" :
            poss = 1
            alado = -1
        else:
            print("elija solo \"i\" para izquierda o \"d\" para derecha")
            return 0

        for ficha in tablero:
            for nummazo in list(range(poss,len(mazoNP),2)):
                fichaArr = generarFichaNP(ficha)
                if fichaArr[poss] == mazoNP[nummazo]:
                    mazoNP[nummazo] = 0
                    mazoNP[nummazo + alado] = 0
                    sumadorP+=1
                    if ficha in tablero:
                        tablero.remove(ficha)

        return sumadorP


def imprimirAvance(mazo,tablero,puntaje):
    print("Mazo: ", mazo)
    print("Tablero: ", tablero)
    print("Puntaje: ", puntaje)

def agregarFichaMazo(mazo):
    indice=0
    while indice <= len(mazo)-1:
        if mazo[indice] == 0:
            mazo[indice] = rd.randint(1,9)
            mazo[indice+1] = rd.randint(1, 9)
            return mazo
        indice+=1
    mazo = np.append(mazo,[rd.randint(1,9),rd.randint(1,9)])
    return mazo

def mostratReporte(usuarios,puntajes):
    usuariosNP = np.array(usuarios)
    puntajesNP = np.array(puntajes)
    nuevoarr= [[],[]]
    for i in range(len(puntajesNP)):
        maxPuntaje = puntajesNP.max()
        theIndex =list(puntajesNP).index(maxPuntaje)
        nuevoarr[1].append(puntajesNP[theIndex])
        nuevoarr[0].append(usuariosNP[theIndex])
        puntajesNP[theIndex] = 0
    
    nuevoarrNP = np.array(nuevoarr)
    print(
        """
+++++REPORTE+++++
JUGADOR / PUNTAJE
        """
    )
    print(np.transpose(nuevoarrNP))
    print(
        """
+++++mejores jugadores+++++
        """
    )
    mejores=[]
    mejor=nuevoarr[1][0]
    for i in range(len(nuevoarr[1])):
      if nuevoarr[1][i] == mejor:
        mejores.append(nuevoarr[0][i])

    print(mejores)